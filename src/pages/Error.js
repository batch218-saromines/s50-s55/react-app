import { Row, Col, Button } from "react-bootstrap";

export default function Error() {
  return (
    <Row>
      <Col className="p-5">
        <h1> 404 - Not Found</h1>
        <p> The page you are looking for cannot be found</p>
        <Button variant="primary" href="http://localhost:3000/">
          Back Home
        </Button>
      </Col>
    </Row>
  );
}
